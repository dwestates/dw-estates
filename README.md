With over fifteen years experience in real estate, serving San Pedro de Alcantara and surrounding areas, DW Estates are the 'go-to' agent for property in the area.

If you're looking for property for sale or to list your property, we deal with direct clients and other real estate agents alike.

We are a professional estate agency specialising in property in San Pedro de Alcantara and the surrounding area, including Guadalmina Baja, Guadalmina Alta, Puerto Banus and Nueva Andalucia.

We have a vast network of agencies that refer clients from a vast spectrum of different nationalities. This means that we can focus on what we do the best - to sell your property in San Pedro de Alcantara.

Our services include property sales, representing both vendors and buyers; as well as development consultancy and advisory services.

If you're looking to sell your property from one of the many popular urbanisations, we have vast knowledge in Los Almendros, El Mirador, Nueva Alcantara, Los Naranjos, Los Jazmines, El Noray, La Gavia, La Rada, La Gaviota, Las Adelfas, Bahia Alcantara, Los Arqueros Beach, Jade Beach and Marques del Duero.